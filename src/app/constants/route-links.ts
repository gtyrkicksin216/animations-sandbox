import { ROUTES } from '@routing/routes';

export const BUTTONS = [
  // { label: 'ViewName', path: ROUTES.NAME },
  { viewName: 'Slider', path: ROUTES.SLIDER },
  { viewName: 'Spinner', path: ROUTES.SPINNER },
  { viewName: 'Shrink', path: ROUTES.SHRINK },
  { viewName: 'Stagger', path: ROUTES.CHILD_STAGGER },
];
